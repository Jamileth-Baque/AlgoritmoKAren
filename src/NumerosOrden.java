import java.util.Scanner;

public class NumerosOrden {

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		int tam; 
		System.out.println("Ingrese la cantidad de numeros a ordenar");
		//Definimos el tama�o del arreglo
		tam = ingreso.nextInt();
		
		int arr[] = new int [tam];
		
		System.out.println("Ingrese los"+tam+" numeros enteros");
		
		for(int i=0; i<tam; i++){
            
			arr [i] = ingreso.nextInt();
		}
		
		int aux= 0 ;
		
		//Ordenamos los numeros
		
		for(int j=0; j<tam; j++){
			
			for(int i=0; i<tam-1; i++){
	            
				
				if (arr[i]> arr[i+1]) {
					aux= arr[i];
					arr[i]=arr[i+1];
					arr[i+1]=aux;
					
					
				}
			}  
			
		}
		
		int orden = 0;
		
		System.out.print("Ver ordenamiento de numeros");
		
		while (orden < tam) {
		
			System.out.print(arr[orden]+" ");
			
			orden++;
			
			
		}
	}

}